﻿namespace Sudoku
{
    partial class MainWindow
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.textBox81 = new System.Windows.Forms.TextBox();
            this.textBox80 = new System.Windows.Forms.TextBox();
            this.textBox79 = new System.Windows.Forms.TextBox();
            this.textBox78 = new System.Windows.Forms.TextBox();
            this.textBox77 = new System.Windows.Forms.TextBox();
            this.textBox76 = new System.Windows.Forms.TextBox();
            this.textBox75 = new System.Windows.Forms.TextBox();
            this.textBox74 = new System.Windows.Forms.TextBox();
            this.textBox73 = new System.Windows.Forms.TextBox();
            this.textBox72 = new System.Windows.Forms.TextBox();
            this.textBox71 = new System.Windows.Forms.TextBox();
            this.textBox70 = new System.Windows.Forms.TextBox();
            this.textBox69 = new System.Windows.Forms.TextBox();
            this.textBox68 = new System.Windows.Forms.TextBox();
            this.textBox67 = new System.Windows.Forms.TextBox();
            this.textBox66 = new System.Windows.Forms.TextBox();
            this.textBox65 = new System.Windows.Forms.TextBox();
            this.textBox64 = new System.Windows.Forms.TextBox();
            this.textBox63 = new System.Windows.Forms.TextBox();
            this.textBox62 = new System.Windows.Forms.TextBox();
            this.textBox61 = new System.Windows.Forms.TextBox();
            this.textBox60 = new System.Windows.Forms.TextBox();
            this.textBox59 = new System.Windows.Forms.TextBox();
            this.textBox58 = new System.Windows.Forms.TextBox();
            this.textBox57 = new System.Windows.Forms.TextBox();
            this.textBox56 = new System.Windows.Forms.TextBox();
            this.textBox55 = new System.Windows.Forms.TextBox();
            this.textBox54 = new System.Windows.Forms.TextBox();
            this.textBox53 = new System.Windows.Forms.TextBox();
            this.textBox52 = new System.Windows.Forms.TextBox();
            this.textBox51 = new System.Windows.Forms.TextBox();
            this.textBox50 = new System.Windows.Forms.TextBox();
            this.textBox49 = new System.Windows.Forms.TextBox();
            this.textBox48 = new System.Windows.Forms.TextBox();
            this.textBox47 = new System.Windows.Forms.TextBox();
            this.textBox46 = new System.Windows.Forms.TextBox();
            this.textBox45 = new System.Windows.Forms.TextBox();
            this.textBox44 = new System.Windows.Forms.TextBox();
            this.textBox43 = new System.Windows.Forms.TextBox();
            this.textBox42 = new System.Windows.Forms.TextBox();
            this.textBox41 = new System.Windows.Forms.TextBox();
            this.textBox40 = new System.Windows.Forms.TextBox();
            this.textBox39 = new System.Windows.Forms.TextBox();
            this.textBox38 = new System.Windows.Forms.TextBox();
            this.textBox37 = new System.Windows.Forms.TextBox();
            this.textBox36 = new System.Windows.Forms.TextBox();
            this.textBox35 = new System.Windows.Forms.TextBox();
            this.textBox34 = new System.Windows.Forms.TextBox();
            this.textBox33 = new System.Windows.Forms.TextBox();
            this.textBox32 = new System.Windows.Forms.TextBox();
            this.textBox31 = new System.Windows.Forms.TextBox();
            this.textBox30 = new System.Windows.Forms.TextBox();
            this.textBox29 = new System.Windows.Forms.TextBox();
            this.textBox28 = new System.Windows.Forms.TextBox();
            this.textBox27 = new System.Windows.Forms.TextBox();
            this.textBox26 = new System.Windows.Forms.TextBox();
            this.textBox25 = new System.Windows.Forms.TextBox();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textConsole = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.startFromNum = new System.Windows.Forms.NumericUpDown();
            this.button2 = new System.Windows.Forms.Button();
            this.HelpCheck = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.LogOn = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.startFromNum)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 11;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.Controls.Add(this.textBox81, 10, 10);
            this.tableLayoutPanel1.Controls.Add(this.textBox80, 9, 10);
            this.tableLayoutPanel1.Controls.Add(this.textBox79, 8, 10);
            this.tableLayoutPanel1.Controls.Add(this.textBox78, 6, 10);
            this.tableLayoutPanel1.Controls.Add(this.textBox77, 5, 10);
            this.tableLayoutPanel1.Controls.Add(this.textBox76, 4, 10);
            this.tableLayoutPanel1.Controls.Add(this.textBox75, 2, 10);
            this.tableLayoutPanel1.Controls.Add(this.textBox74, 1, 10);
            this.tableLayoutPanel1.Controls.Add(this.textBox73, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.textBox72, 10, 9);
            this.tableLayoutPanel1.Controls.Add(this.textBox71, 9, 9);
            this.tableLayoutPanel1.Controls.Add(this.textBox70, 8, 9);
            this.tableLayoutPanel1.Controls.Add(this.textBox69, 6, 9);
            this.tableLayoutPanel1.Controls.Add(this.textBox68, 5, 9);
            this.tableLayoutPanel1.Controls.Add(this.textBox67, 4, 9);
            this.tableLayoutPanel1.Controls.Add(this.textBox66, 2, 9);
            this.tableLayoutPanel1.Controls.Add(this.textBox65, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.textBox64, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.textBox63, 10, 8);
            this.tableLayoutPanel1.Controls.Add(this.textBox62, 9, 8);
            this.tableLayoutPanel1.Controls.Add(this.textBox61, 8, 8);
            this.tableLayoutPanel1.Controls.Add(this.textBox60, 6, 8);
            this.tableLayoutPanel1.Controls.Add(this.textBox59, 5, 8);
            this.tableLayoutPanel1.Controls.Add(this.textBox58, 4, 8);
            this.tableLayoutPanel1.Controls.Add(this.textBox57, 2, 8);
            this.tableLayoutPanel1.Controls.Add(this.textBox56, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.textBox55, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.textBox54, 10, 6);
            this.tableLayoutPanel1.Controls.Add(this.textBox53, 9, 6);
            this.tableLayoutPanel1.Controls.Add(this.textBox52, 8, 6);
            this.tableLayoutPanel1.Controls.Add(this.textBox51, 6, 6);
            this.tableLayoutPanel1.Controls.Add(this.textBox50, 5, 6);
            this.tableLayoutPanel1.Controls.Add(this.textBox49, 4, 6);
            this.tableLayoutPanel1.Controls.Add(this.textBox48, 2, 6);
            this.tableLayoutPanel1.Controls.Add(this.textBox47, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.textBox46, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.textBox45, 10, 5);
            this.tableLayoutPanel1.Controls.Add(this.textBox44, 9, 5);
            this.tableLayoutPanel1.Controls.Add(this.textBox43, 8, 5);
            this.tableLayoutPanel1.Controls.Add(this.textBox42, 6, 5);
            this.tableLayoutPanel1.Controls.Add(this.textBox41, 5, 5);
            this.tableLayoutPanel1.Controls.Add(this.textBox40, 4, 5);
            this.tableLayoutPanel1.Controls.Add(this.textBox39, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.textBox38, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.textBox37, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.textBox36, 10, 4);
            this.tableLayoutPanel1.Controls.Add(this.textBox35, 9, 4);
            this.tableLayoutPanel1.Controls.Add(this.textBox34, 8, 4);
            this.tableLayoutPanel1.Controls.Add(this.textBox33, 6, 4);
            this.tableLayoutPanel1.Controls.Add(this.textBox32, 5, 4);
            this.tableLayoutPanel1.Controls.Add(this.textBox31, 4, 4);
            this.tableLayoutPanel1.Controls.Add(this.textBox30, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.textBox29, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.textBox28, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.textBox27, 10, 2);
            this.tableLayoutPanel1.Controls.Add(this.textBox26, 9, 2);
            this.tableLayoutPanel1.Controls.Add(this.textBox25, 8, 2);
            this.tableLayoutPanel1.Controls.Add(this.textBox24, 6, 2);
            this.tableLayoutPanel1.Controls.Add(this.textBox23, 5, 2);
            this.tableLayoutPanel1.Controls.Add(this.textBox22, 4, 2);
            this.tableLayoutPanel1.Controls.Add(this.textBox21, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.textBox20, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.textBox19, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.textBox18, 10, 1);
            this.tableLayoutPanel1.Controls.Add(this.textBox17, 9, 1);
            this.tableLayoutPanel1.Controls.Add(this.textBox16, 8, 1);
            this.tableLayoutPanel1.Controls.Add(this.textBox15, 6, 1);
            this.tableLayoutPanel1.Controls.Add(this.textBox14, 5, 1);
            this.tableLayoutPanel1.Controls.Add(this.textBox13, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.textBox12, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.textBox11, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.textBox10, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.textBox9, 10, 0);
            this.tableLayoutPanel1.Controls.Add(this.textBox8, 9, 0);
            this.tableLayoutPanel1.Controls.Add(this.textBox7, 8, 0);
            this.tableLayoutPanel1.Controls.Add(this.textBox6, 6, 0);
            this.tableLayoutPanel1.Controls.Add(this.textBox5, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.textBox4, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.textBox3, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.textBox2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.textBox1, 0, 0);
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 60);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 11;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 11F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 11F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(341, 273);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // textBox81
            // 
            this.textBox81.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox81.Location = new System.Drawing.Point(307, 241);
            this.textBox81.Name = "textBox81";
            this.textBox81.Size = new System.Drawing.Size(31, 26);
            this.textBox81.TabIndex = 80;
            this.textBox81.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox80
            // 
            this.textBox80.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox80.Location = new System.Drawing.Point(271, 241);
            this.textBox80.Name = "textBox80";
            this.textBox80.Size = new System.Drawing.Size(30, 26);
            this.textBox80.TabIndex = 79;
            this.textBox80.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox79
            // 
            this.textBox79.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox79.Location = new System.Drawing.Point(235, 241);
            this.textBox79.Name = "textBox79";
            this.textBox79.Size = new System.Drawing.Size(30, 26);
            this.textBox79.TabIndex = 78;
            this.textBox79.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox78
            // 
            this.textBox78.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox78.Location = new System.Drawing.Point(191, 241);
            this.textBox78.Name = "textBox78";
            this.textBox78.Size = new System.Drawing.Size(30, 26);
            this.textBox78.TabIndex = 77;
            this.textBox78.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox77
            // 
            this.textBox77.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox77.Location = new System.Drawing.Point(155, 241);
            this.textBox77.Name = "textBox77";
            this.textBox77.Size = new System.Drawing.Size(30, 26);
            this.textBox77.TabIndex = 76;
            this.textBox77.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox76
            // 
            this.textBox76.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox76.Location = new System.Drawing.Point(119, 241);
            this.textBox76.Name = "textBox76";
            this.textBox76.Size = new System.Drawing.Size(30, 26);
            this.textBox76.TabIndex = 75;
            this.textBox76.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox75
            // 
            this.textBox75.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox75.Location = new System.Drawing.Point(75, 241);
            this.textBox75.Name = "textBox75";
            this.textBox75.Size = new System.Drawing.Size(30, 26);
            this.textBox75.TabIndex = 74;
            this.textBox75.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox74
            // 
            this.textBox74.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox74.Location = new System.Drawing.Point(39, 241);
            this.textBox74.Name = "textBox74";
            this.textBox74.Size = new System.Drawing.Size(30, 26);
            this.textBox74.TabIndex = 73;
            this.textBox74.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox73
            // 
            this.textBox73.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox73.Location = new System.Drawing.Point(3, 241);
            this.textBox73.Name = "textBox73";
            this.textBox73.Size = new System.Drawing.Size(30, 26);
            this.textBox73.TabIndex = 72;
            this.textBox73.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox72
            // 
            this.textBox72.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox72.Location = new System.Drawing.Point(307, 214);
            this.textBox72.Name = "textBox72";
            this.textBox72.Size = new System.Drawing.Size(31, 26);
            this.textBox72.TabIndex = 71;
            this.textBox72.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox71
            // 
            this.textBox71.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox71.Location = new System.Drawing.Point(271, 214);
            this.textBox71.Name = "textBox71";
            this.textBox71.Size = new System.Drawing.Size(30, 26);
            this.textBox71.TabIndex = 70;
            this.textBox71.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox70
            // 
            this.textBox70.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox70.Location = new System.Drawing.Point(235, 214);
            this.textBox70.Name = "textBox70";
            this.textBox70.Size = new System.Drawing.Size(30, 26);
            this.textBox70.TabIndex = 69;
            this.textBox70.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox69
            // 
            this.textBox69.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox69.Location = new System.Drawing.Point(191, 214);
            this.textBox69.Name = "textBox69";
            this.textBox69.Size = new System.Drawing.Size(30, 26);
            this.textBox69.TabIndex = 68;
            this.textBox69.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox68
            // 
            this.textBox68.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox68.Location = new System.Drawing.Point(155, 214);
            this.textBox68.Name = "textBox68";
            this.textBox68.Size = new System.Drawing.Size(30, 26);
            this.textBox68.TabIndex = 67;
            this.textBox68.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox67
            // 
            this.textBox67.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox67.Location = new System.Drawing.Point(119, 214);
            this.textBox67.Name = "textBox67";
            this.textBox67.Size = new System.Drawing.Size(30, 26);
            this.textBox67.TabIndex = 66;
            this.textBox67.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox66
            // 
            this.textBox66.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox66.Location = new System.Drawing.Point(75, 214);
            this.textBox66.Name = "textBox66";
            this.textBox66.Size = new System.Drawing.Size(30, 26);
            this.textBox66.TabIndex = 65;
            this.textBox66.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox65
            // 
            this.textBox65.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox65.Location = new System.Drawing.Point(39, 214);
            this.textBox65.Name = "textBox65";
            this.textBox65.Size = new System.Drawing.Size(30, 26);
            this.textBox65.TabIndex = 64;
            this.textBox65.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox64
            // 
            this.textBox64.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox64.Location = new System.Drawing.Point(3, 214);
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new System.Drawing.Size(30, 26);
            this.textBox64.TabIndex = 63;
            this.textBox64.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox63
            // 
            this.textBox63.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox63.Location = new System.Drawing.Point(307, 187);
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new System.Drawing.Size(31, 26);
            this.textBox63.TabIndex = 62;
            this.textBox63.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox62
            // 
            this.textBox62.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox62.Location = new System.Drawing.Point(271, 187);
            this.textBox62.Name = "textBox62";
            this.textBox62.Size = new System.Drawing.Size(30, 26);
            this.textBox62.TabIndex = 61;
            this.textBox62.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox61
            // 
            this.textBox61.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox61.Location = new System.Drawing.Point(235, 187);
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new System.Drawing.Size(30, 26);
            this.textBox61.TabIndex = 60;
            this.textBox61.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox60
            // 
            this.textBox60.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox60.Location = new System.Drawing.Point(191, 187);
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new System.Drawing.Size(30, 26);
            this.textBox60.TabIndex = 59;
            this.textBox60.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox59
            // 
            this.textBox59.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox59.Location = new System.Drawing.Point(155, 187);
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new System.Drawing.Size(30, 26);
            this.textBox59.TabIndex = 58;
            this.textBox59.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox58
            // 
            this.textBox58.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox58.Location = new System.Drawing.Point(119, 187);
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new System.Drawing.Size(30, 26);
            this.textBox58.TabIndex = 57;
            this.textBox58.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox57
            // 
            this.textBox57.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox57.Location = new System.Drawing.Point(75, 187);
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new System.Drawing.Size(30, 26);
            this.textBox57.TabIndex = 56;
            this.textBox57.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox56
            // 
            this.textBox56.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox56.Location = new System.Drawing.Point(39, 187);
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new System.Drawing.Size(30, 26);
            this.textBox56.TabIndex = 55;
            this.textBox56.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox55
            // 
            this.textBox55.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox55.Location = new System.Drawing.Point(3, 187);
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new System.Drawing.Size(30, 26);
            this.textBox55.TabIndex = 54;
            this.textBox55.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox54
            // 
            this.textBox54.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox54.Location = new System.Drawing.Point(307, 149);
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new System.Drawing.Size(31, 26);
            this.textBox54.TabIndex = 53;
            this.textBox54.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox53
            // 
            this.textBox53.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox53.Location = new System.Drawing.Point(271, 149);
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new System.Drawing.Size(30, 26);
            this.textBox53.TabIndex = 52;
            this.textBox53.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox52
            // 
            this.textBox52.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox52.Location = new System.Drawing.Point(235, 149);
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new System.Drawing.Size(30, 26);
            this.textBox52.TabIndex = 51;
            this.textBox52.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox51
            // 
            this.textBox51.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox51.Location = new System.Drawing.Point(191, 149);
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new System.Drawing.Size(30, 26);
            this.textBox51.TabIndex = 50;
            this.textBox51.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox50
            // 
            this.textBox50.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox50.Location = new System.Drawing.Point(155, 149);
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new System.Drawing.Size(30, 26);
            this.textBox50.TabIndex = 49;
            this.textBox50.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox49
            // 
            this.textBox49.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox49.Location = new System.Drawing.Point(119, 149);
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new System.Drawing.Size(30, 26);
            this.textBox49.TabIndex = 48;
            this.textBox49.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox48
            // 
            this.textBox48.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox48.Location = new System.Drawing.Point(75, 149);
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new System.Drawing.Size(30, 26);
            this.textBox48.TabIndex = 47;
            this.textBox48.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox47
            // 
            this.textBox47.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox47.Location = new System.Drawing.Point(39, 149);
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new System.Drawing.Size(30, 26);
            this.textBox47.TabIndex = 46;
            this.textBox47.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox46
            // 
            this.textBox46.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox46.Location = new System.Drawing.Point(3, 149);
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new System.Drawing.Size(30, 26);
            this.textBox46.TabIndex = 45;
            this.textBox46.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox45
            // 
            this.textBox45.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox45.Location = new System.Drawing.Point(307, 122);
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new System.Drawing.Size(31, 26);
            this.textBox45.TabIndex = 44;
            this.textBox45.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox44
            // 
            this.textBox44.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox44.Location = new System.Drawing.Point(271, 122);
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new System.Drawing.Size(30, 26);
            this.textBox44.TabIndex = 43;
            this.textBox44.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox43
            // 
            this.textBox43.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox43.Location = new System.Drawing.Point(235, 122);
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new System.Drawing.Size(30, 26);
            this.textBox43.TabIndex = 42;
            this.textBox43.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox42
            // 
            this.textBox42.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox42.Location = new System.Drawing.Point(191, 122);
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new System.Drawing.Size(30, 26);
            this.textBox42.TabIndex = 41;
            this.textBox42.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox41
            // 
            this.textBox41.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox41.Location = new System.Drawing.Point(155, 122);
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new System.Drawing.Size(30, 26);
            this.textBox41.TabIndex = 40;
            this.textBox41.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox40
            // 
            this.textBox40.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox40.Location = new System.Drawing.Point(119, 122);
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new System.Drawing.Size(30, 26);
            this.textBox40.TabIndex = 39;
            this.textBox40.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox39
            // 
            this.textBox39.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox39.Location = new System.Drawing.Point(75, 122);
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new System.Drawing.Size(30, 26);
            this.textBox39.TabIndex = 38;
            this.textBox39.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox38
            // 
            this.textBox38.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox38.Location = new System.Drawing.Point(39, 122);
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new System.Drawing.Size(30, 26);
            this.textBox38.TabIndex = 37;
            this.textBox38.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox37
            // 
            this.textBox37.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox37.Location = new System.Drawing.Point(3, 122);
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new System.Drawing.Size(30, 26);
            this.textBox37.TabIndex = 36;
            this.textBox37.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox36
            // 
            this.textBox36.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox36.Location = new System.Drawing.Point(307, 95);
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new System.Drawing.Size(31, 26);
            this.textBox36.TabIndex = 35;
            this.textBox36.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox35
            // 
            this.textBox35.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox35.Location = new System.Drawing.Point(271, 95);
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new System.Drawing.Size(30, 26);
            this.textBox35.TabIndex = 34;
            this.textBox35.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox34
            // 
            this.textBox34.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox34.Location = new System.Drawing.Point(235, 95);
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new System.Drawing.Size(30, 26);
            this.textBox34.TabIndex = 33;
            this.textBox34.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox33
            // 
            this.textBox33.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox33.Location = new System.Drawing.Point(191, 95);
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new System.Drawing.Size(30, 26);
            this.textBox33.TabIndex = 32;
            this.textBox33.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox32
            // 
            this.textBox32.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox32.Location = new System.Drawing.Point(155, 95);
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new System.Drawing.Size(30, 26);
            this.textBox32.TabIndex = 31;
            this.textBox32.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox31
            // 
            this.textBox31.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox31.Location = new System.Drawing.Point(119, 95);
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new System.Drawing.Size(30, 26);
            this.textBox31.TabIndex = 30;
            this.textBox31.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox30
            // 
            this.textBox30.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox30.Location = new System.Drawing.Point(75, 95);
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new System.Drawing.Size(30, 26);
            this.textBox30.TabIndex = 29;
            this.textBox30.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox29
            // 
            this.textBox29.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox29.Location = new System.Drawing.Point(39, 95);
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new System.Drawing.Size(30, 26);
            this.textBox29.TabIndex = 28;
            this.textBox29.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox28
            // 
            this.textBox28.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox28.Location = new System.Drawing.Point(3, 95);
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new System.Drawing.Size(30, 26);
            this.textBox28.TabIndex = 27;
            this.textBox28.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox27
            // 
            this.textBox27.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox27.Location = new System.Drawing.Point(307, 57);
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new System.Drawing.Size(31, 26);
            this.textBox27.TabIndex = 26;
            this.textBox27.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox26
            // 
            this.textBox26.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox26.Location = new System.Drawing.Point(271, 57);
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new System.Drawing.Size(30, 26);
            this.textBox26.TabIndex = 25;
            this.textBox26.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox25
            // 
            this.textBox25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox25.Location = new System.Drawing.Point(235, 57);
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new System.Drawing.Size(30, 26);
            this.textBox25.TabIndex = 24;
            this.textBox25.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox24
            // 
            this.textBox24.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox24.Location = new System.Drawing.Point(191, 57);
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new System.Drawing.Size(30, 26);
            this.textBox24.TabIndex = 23;
            this.textBox24.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox23
            // 
            this.textBox23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox23.Location = new System.Drawing.Point(155, 57);
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new System.Drawing.Size(30, 26);
            this.textBox23.TabIndex = 22;
            this.textBox23.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox22
            // 
            this.textBox22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox22.Location = new System.Drawing.Point(119, 57);
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new System.Drawing.Size(30, 26);
            this.textBox22.TabIndex = 21;
            this.textBox22.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox21
            // 
            this.textBox21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox21.Location = new System.Drawing.Point(75, 57);
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new System.Drawing.Size(30, 26);
            this.textBox21.TabIndex = 20;
            this.textBox21.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox20
            // 
            this.textBox20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox20.Location = new System.Drawing.Point(39, 57);
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new System.Drawing.Size(30, 26);
            this.textBox20.TabIndex = 19;
            this.textBox20.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox19
            // 
            this.textBox19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox19.Location = new System.Drawing.Point(3, 57);
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(30, 26);
            this.textBox19.TabIndex = 18;
            this.textBox19.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox18
            // 
            this.textBox18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox18.Location = new System.Drawing.Point(307, 30);
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(31, 26);
            this.textBox18.TabIndex = 17;
            this.textBox18.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox17
            // 
            this.textBox17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox17.Location = new System.Drawing.Point(271, 30);
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(30, 26);
            this.textBox17.TabIndex = 16;
            this.textBox17.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox16
            // 
            this.textBox16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox16.Location = new System.Drawing.Point(235, 30);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(30, 26);
            this.textBox16.TabIndex = 15;
            this.textBox16.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox15
            // 
            this.textBox15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox15.Location = new System.Drawing.Point(191, 30);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(30, 26);
            this.textBox15.TabIndex = 14;
            this.textBox15.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox14
            // 
            this.textBox14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox14.Location = new System.Drawing.Point(155, 30);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(30, 26);
            this.textBox14.TabIndex = 13;
            this.textBox14.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox13
            // 
            this.textBox13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox13.Location = new System.Drawing.Point(119, 30);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(30, 26);
            this.textBox13.TabIndex = 12;
            this.textBox13.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox12
            // 
            this.textBox12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox12.Location = new System.Drawing.Point(75, 30);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(30, 26);
            this.textBox12.TabIndex = 11;
            this.textBox12.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox11
            // 
            this.textBox11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox11.Location = new System.Drawing.Point(39, 30);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(30, 26);
            this.textBox11.TabIndex = 10;
            this.textBox11.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox10
            // 
            this.textBox10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox10.Location = new System.Drawing.Point(3, 30);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(30, 26);
            this.textBox10.TabIndex = 9;
            this.textBox10.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox9
            // 
            this.textBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox9.Location = new System.Drawing.Point(307, 3);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(31, 26);
            this.textBox9.TabIndex = 8;
            this.textBox9.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox8
            // 
            this.textBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox8.Location = new System.Drawing.Point(271, 3);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(30, 26);
            this.textBox8.TabIndex = 7;
            this.textBox8.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox7
            // 
            this.textBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox7.Location = new System.Drawing.Point(235, 3);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(30, 26);
            this.textBox7.TabIndex = 6;
            this.textBox7.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox6
            // 
            this.textBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox6.Location = new System.Drawing.Point(191, 3);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(30, 26);
            this.textBox6.TabIndex = 5;
            this.textBox6.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox5
            // 
            this.textBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox5.Location = new System.Drawing.Point(155, 3);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(30, 26);
            this.textBox5.TabIndex = 4;
            this.textBox5.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox4
            // 
            this.textBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox4.Location = new System.Drawing.Point(119, 3);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(30, 26);
            this.textBox4.TabIndex = 3;
            this.textBox4.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox3
            // 
            this.textBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox3.Location = new System.Drawing.Point(75, 3);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(30, 26);
            this.textBox3.TabIndex = 2;
            this.textBox3.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox2
            // 
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox2.Location = new System.Drawing.Point(39, 3);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(30, 26);
            this.textBox2.TabIndex = 1;
            this.textBox2.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox1.Location = new System.Drawing.Point(3, 3);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(30, 26);
            this.textBox1.TabIndex = 0;
            this.textBox1.Leave += new System.EventHandler(this.textBox_Leave);
            // 
            // textConsole
            // 
            this.textConsole.Location = new System.Drawing.Point(12, 357);
            this.textConsole.Multiline = true;
            this.textConsole.Name = "textConsole";
            this.textConsole.ReadOnly = true;
            this.textConsole.Size = new System.Drawing.Size(534, 89);
            this.textConsole.TabIndex = 1;
            this.textConsole.Text = "Вывод:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(386, 60);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(125, 56);
            this.button1.TabIndex = 2;
            this.button1.Text = "Решить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // startFromNum
            // 
            this.startFromNum.Location = new System.Drawing.Point(467, 227);
            this.startFromNum.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.startFromNum.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.startFromNum.Name = "startFromNum";
            this.startFromNum.Size = new System.Drawing.Size(66, 20);
            this.startFromNum.TabIndex = 3;
            this.startFromNum.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(386, 138);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(125, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "Очистить";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // HelpCheck
            // 
            this.HelpCheck.AutoSize = true;
            this.HelpCheck.Location = new System.Drawing.Point(370, 256);
            this.HelpCheck.Name = "HelpCheck";
            this.HelpCheck.Size = new System.Drawing.Size(152, 17);
            this.HelpCheck.TabIndex = 5;
            this.HelpCheck.Text = "Вкл. проверку при вводе";
            this.HelpCheck.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(407, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "Введите задание в ячейки ниже и нажмите кнопку \"Решить\"";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(367, 229);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Начать подбор с:";
            // 
            // LogOn
            // 
            this.LogOn.AutoSize = true;
            this.LogOn.Location = new System.Drawing.Point(370, 274);
            this.LogOn.Name = "LogOn";
            this.LogOn.Size = new System.Drawing.Size(121, 17);
            this.LogOn.TabIndex = 8;
            this.LogOn.Text = "Вести лог подбора";
            this.LogOn.UseVisualStyleBackColor = true;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(558, 458);
            this.Controls.Add(this.LogOn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.HelpCheck);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.startFromNum);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textConsole);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "MainWindow";
            this.Text = "Sudoku решебник";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.startFromNum)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox textBox81;
        private System.Windows.Forms.TextBox textBox80;
        private System.Windows.Forms.TextBox textBox79;
        private System.Windows.Forms.TextBox textBox78;
        private System.Windows.Forms.TextBox textBox77;
        private System.Windows.Forms.TextBox textBox76;
        private System.Windows.Forms.TextBox textBox75;
        private System.Windows.Forms.TextBox textBox74;
        private System.Windows.Forms.TextBox textBox73;
        private System.Windows.Forms.TextBox textBox72;
        private System.Windows.Forms.TextBox textBox71;
        private System.Windows.Forms.TextBox textBox70;
        private System.Windows.Forms.TextBox textBox69;
        private System.Windows.Forms.TextBox textBox68;
        private System.Windows.Forms.TextBox textBox67;
        private System.Windows.Forms.TextBox textBox66;
        private System.Windows.Forms.TextBox textBox65;
        private System.Windows.Forms.TextBox textBox64;
        private System.Windows.Forms.TextBox textBox63;
        private System.Windows.Forms.TextBox textBox62;
        private System.Windows.Forms.TextBox textBox61;
        private System.Windows.Forms.TextBox textBox60;
        private System.Windows.Forms.TextBox textBox59;
        private System.Windows.Forms.TextBox textBox58;
        private System.Windows.Forms.TextBox textBox57;
        private System.Windows.Forms.TextBox textBox56;
        private System.Windows.Forms.TextBox textBox55;
        private System.Windows.Forms.TextBox textBox54;
        private System.Windows.Forms.TextBox textBox53;
        private System.Windows.Forms.TextBox textBox52;
        private System.Windows.Forms.TextBox textBox51;
        private System.Windows.Forms.TextBox textBox50;
        private System.Windows.Forms.TextBox textBox49;
        private System.Windows.Forms.TextBox textBox48;
        private System.Windows.Forms.TextBox textBox47;
        private System.Windows.Forms.TextBox textBox46;
        private System.Windows.Forms.TextBox textBox45;
        private System.Windows.Forms.TextBox textBox44;
        private System.Windows.Forms.TextBox textBox43;
        private System.Windows.Forms.TextBox textBox42;
        private System.Windows.Forms.TextBox textBox41;
        private System.Windows.Forms.TextBox textBox40;
        private System.Windows.Forms.TextBox textBox39;
        private System.Windows.Forms.TextBox textBox38;
        private System.Windows.Forms.TextBox textBox37;
        private System.Windows.Forms.TextBox textBox36;
        private System.Windows.Forms.TextBox textBox35;
        private System.Windows.Forms.TextBox textBox34;
        private System.Windows.Forms.TextBox textBox33;
        private System.Windows.Forms.TextBox textBox32;
        private System.Windows.Forms.TextBox textBox31;
        private System.Windows.Forms.TextBox textBox30;
        private System.Windows.Forms.TextBox textBox29;
        private System.Windows.Forms.TextBox textBox28;
        private System.Windows.Forms.TextBox textBox27;
        private System.Windows.Forms.TextBox textBox26;
        private System.Windows.Forms.TextBox textBox25;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textConsole;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.NumericUpDown startFromNum;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.CheckBox HelpCheck;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox LogOn;


    }
}

