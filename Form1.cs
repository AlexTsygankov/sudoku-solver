﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Sudoku
{
    public partial class MainWindow : Form
    {
        private int[,] iSudokuArr = new int[9,9];
        private StringBuilder sOutput = new StringBuilder();
        private StreamWriter SW;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void textBox_Leave(object sender, EventArgs e)
        {
            TextBox txt = (TextBox)sender;
            int iNum = 0;
            if (!String.IsNullOrEmpty(txt.Text) && txt.Text[0] == ' ') iNum = 0;
            else if (!Int32.TryParse(txt.Text, out iNum))
            {
                if (HelpCheck.Checked)
                    MessageBox.Show($"Ошибка. '{txt.Text}' не является допустимым числом или не входит в диапазон (0-9)");
                txt.Text = " ";
                return;
            }
            else if (iNum > 0 && iNum <= 9)
            {
                Point cell = GetCellIdFromTextBoxName(txt);
                iSudokuArr[cell.Y, cell.X] = iNum;
                if (HelpCheck.Checked)
                {
                    textConsole.Text = "Вывод:";
                    if (!CheckRulesAndHilightHints(ref cell, ref txt)) return;
                }
            }
           // else if (iNum != 0) MessageBox.Show("Не верно введено число. Допустимый диапазон(0-9)");
            txt.BackColor = Color.White;
        }
        private bool CheckRules(Point pCheck)
        {
            int[] buffer = Get3by3SquareOf(ref pCheck);
            
            if (!OnlyUniqElmsIn(buffer)) return false; 

            // проверка в строке
            for (int i = 0; i < 9; i++)
                buffer[i] = iSudokuArr[pCheck.Y, i];
            if (!OnlyUniqElmsIn(buffer)) return false;
            
            // Collumn
            for (int i = 0; i < 9; i++)
                buffer[i] = iSudokuArr[i, pCheck.X];
            if (!OnlyUniqElmsIn(buffer))
                return false;
            return true;
        }
        private bool CheckRulesAndHilightHints(ref Point pCheck, ref TextBox txbx)
        {
            //Point pCheck = GetCellIdFromTextBoxName(txbx);
            int[] buffer = Get3by3SquareOf(ref pCheck);

            if (!OnlyUniqElmsIn(buffer))
            {
                txbx.BackColor = Color.Red;
                textConsole.Text += String.Format("\nЧисло {0} уже есть в этом блоке 3х3", iSudokuArr[pCheck.Y, pCheck.X]);
                return false;
            }
            else txbx.BackColor = Color.White;

            // проверка в строке
            for (int i = 0; i < 9; i++)
                buffer[i] = iSudokuArr[pCheck.Y, i];

            if (!OnlyUniqElmsIn(buffer))
            {
                txbx.BackColor = Color.Red;
                textConsole.Text += String.Format("\nЧисло {0} уже есть в этой строке", iSudokuArr[pCheck.Y, pCheck.X]);
                return false;
            }
            else txbx.BackColor = Color.White;

            // Collumn
            for (int i = 0; i < 9; i++)
                buffer[i] = iSudokuArr[i, pCheck.X];
            if (!OnlyUniqElmsIn(buffer))
            {
                txbx.BackColor = Color.Red;
                textConsole.Text += String.Format("\nЧисло {0} уже есть в этом столбце", iSudokuArr[pCheck.Y, pCheck.X]);
                return false;
            }
            else txbx.BackColor = Color.White;
            return true;
        }

        private int[] Get3by3SquareOf(ref Point pCheck)
        {
            int[] buffer = new int[9];

            // Check on squares
            Point pSqrMain = new Point((pCheck.X / 3) * 3 + 1, (pCheck.Y / 3) * 3 + 1);

            // 3x3
            for (int i = pSqrMain.Y - 1, k = 0; i <= pSqrMain.Y + 1; i++)
                for (int j = pSqrMain.X - 1; j <= pSqrMain.X + 1; j++)
                {
                    buffer[k] = iSudokuArr[i, j];
                    k++;
                }

            return buffer;
        }

        /// <summary>
        /// Sequence got only unique elms
        /// </summary>
        /// <param name="seq"></param>
        /// <returns></returns>
        private bool OnlyUniqElmsIn(int[] seq)
        {
            Array.Sort(seq);
            for (int i = 0; i < seq.Length-1; i++)
                if (seq[i] == seq[i + 1] && seq[i] != 0) return false;
            
            return true;
        }
        private Point GetCellIdFromTextBoxName(TextBox txt)
        {
            Point pt = new Point();
            pt.X = Convert.ToInt32(txt.Name.Remove(0, 7)); // remove textBox
            pt.Y = pt.X / 9;
            pt.X = pt.X % 9 -1;

            if (pt.X  < 0)  // first line
            {
                pt.X = 8;
                pt.Y--;
            }
            return pt;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            foreach (TextBox tx in tableLayoutPanel1.Controls)
            {
                Point cell = GetCellIdFromTextBoxName(tx);
                //int iNum = 0;
                //try
                //{
                //    iNum = Convert.ToInt32(tx.Text);
                //}
                //catch (SystemException ee)
                //{
                //    tx.Text = " ";
                //}
                //iSudokuArr[cell.Y, cell.X] = iNum;
                int iNum;
                iSudokuArr[cell.Y, cell.X] = Int32.TryParse(tx.Text, out iNum) ? iNum : 0;
            }
            /***************************/

            /**/
            if(LogOn.Checked)
                SW = new StreamWriter("Log.txt");

            if (TryFitInEmptyPlaceAndStartFrom((int)startFromNum.Value))
                MessageBox.Show("Решение найдено");
            else MessageBox.Show("Нет решения");
            
            if(LogOn.Checked) SW.Close();
            /**/

            /***************************/
            foreach (TextBox tx in tableLayoutPanel1.Controls)
            {
                Point cell = GetCellIdFromTextBoxName(tx);
                if (iSudokuArr[cell.Y, cell.X] == 0) tx.Text = " ";
                else tx.Text = iSudokuArr[cell.Y, cell.X].ToString();
            }            
        }
        private bool TryFitInEmptyPlaceAndStartFrom(int num)
        {
            // Поиск первой незаполненной ячейки
            Point pt = FindFirstFreePos();

            if (pt.X >= 0) // Есть свободные позиции
            {
                while (num <= 9)
                {
                    if (LogOn.Checked)
                    {
                        sOutput.Append(num.ToString());
                        if (sOutput.Length % 3 == 0) sOutput.Append(" ");

                        SW.WriteLine(sOutput.ToString());
                    }

                    iSudokuArr[pt.Y, pt.X] = num;
                    // try num on than place. If it fit in, than fill next place
                    if (CheckRules(pt))
                        if (TryFitInEmptyPlaceAndStartFrom(1)) return true;

                    if (LogOn.Checked)
                    {
                        if (sOutput.Length % 4 == 0) sOutput = sOutput.Remove(sOutput.Length - 2, 2);
                        else sOutput = sOutput.Remove(sOutput.Length - 1, 1);
                    }
                   
                    // try next num on that place
                    iSudokuArr[pt.Y, pt.X] = 0;
                    num++;
                }
            }
            else return true;
            return false;
        }

        private Point FindFirstFreePos()
        {
            for (int j = 0; j < 9; j++)
            {
                for (int i = 0; i < 9; i++)
                {
                    if (iSudokuArr[j, i] == 0)
                    {
                        return new Point(i, j);
                    }
                }
                
            }
            return new Point(-1, -1);
        }
        private void button2_Click(object sender, EventArgs e)
        {
            // clear all
            foreach (TextBox tb in tableLayoutPanel1.Controls)
                tb.Text = " ";

            for (int j = 0; j < 9; j++)
                for (int i = 0; i < 9; i++)
                    iSudokuArr[j, i] = 0;
        }
    }
}
